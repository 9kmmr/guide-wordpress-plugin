# guide-wordpress-plugin

simple guide to build a wordpress plugin using wppb.me template

# FIRST THING #
1. mở site : wppb.me và tạo 1 wordpress plugin (cần điền đầy đủ thông tin) rồi down load về cài
2. copy các file core trong GUIDE file vào " {tên_plugin}/include/{class-tên_plugin-loader.php} "
trong đó {tên_plugin} là tên bạn vừa đặt lúc nãy.
3. nếu làm việc bên backend - tức phần admin dashboard thì làm việc với folder "admin" và trong method (function ) "define_admin_hooks" trong /includes/{class-tên_plugin.php}.
4. nếu làm việc bên frontend - tức phần client ngoài thì làm việc với folder "public" và trong method (function ) "define_public_hooks" trong /includes/{class-tên_plugin.php}.

##  SECOND THING ##
1. mở folder admin / public trong đó có file GUIDE để xem có thể làm được những gì với từng phần và hướng dẫn làm việc