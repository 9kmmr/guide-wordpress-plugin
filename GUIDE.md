# Khi cần remove action

## - thêm các thuộc tính:##
    protected $actions_remove;
## - thêm vào constructor:##
    $this->actions_remove = array();
## - thêm các phương thức (các function) sau :##
    public function remove_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->actions_remove = $this->add( $this->actions, $hook, $component, $callback, $priority, $accepted_args );
	}

# Khi cần remove filter
## - thêm các thuộc tính: ##
    protected $filters_remove;
## - thêm vào constructor:##
    $this->filters_remove = array();
## - thêm các phương thức (các function) sau :##
    public function remove_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
		$this->filters_remove = $this->add( $this->filters, $hook, $component, $callback, $priority, $accepted_args );
	}
# Khi cần add shortcode
## - thêm các thuộc tính: ##
    protected $shortcodes;
## - thêm vào constructor:##
    $this->shortcodes = array();
## - thêm các phương thức (các function) sau :##
    public function add_shortcode( $tag, $component, $callback, $priority = 10, $accepted_args = 2 ) {
		$this->shortcodes = $this->add( $this->shortcodes, $tag, $component, $callback, $priority, $accepted_args );
	}

# CUỐI CÙNG #

## - Thay đổi phương thức run ( function run()) bằng đoạn này : ##

    public function run() {
		if ($this->filters) {

			foreach ( $this->filters as $hook ) {
				add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
			}
		}
		if ($this->actions) {

			foreach ( $this->actions as $hook ) {
				add_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
			}
		}
		if ( $this->filters_remove) {

			foreach ( $this->filters_remove as $hook ) {
				remove_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
			}
		}
		if ($this->actions_remove) {

			foreach ( $this->actions_remove as $hook ) {
				remove_action( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
			}
		}
		if ($this->shortcodes) {

			foreach ( $this->shortcodes as $hook ) {
				add_shortcode(  $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );
			}
		}

	}

