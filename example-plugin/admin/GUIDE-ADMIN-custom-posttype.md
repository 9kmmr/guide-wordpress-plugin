# RENDER HIỂN THỊ CHO CUSTOM POSTTYPE, TẠO SORTABLE COLUMN  CHO META TRONG POSTTYPE #

1. tùy chọn hiển thị cho các column trong custom posttype

*   add action với từng loại post type với action hook 'manage_{tên post type}_posts_columns'

    // columns for frame 
	$this->loader->add_action( 'manage_frame_posts_columns' ,$plugin_admin, 'custom_columns_frame' );

	// columns for artist
	$this->loader->add_action( 'manage_artist_posts_columns' ,$plugin_admin, 'custom_columns_artist' );

*  function thêm column cho post type 
    - ví dụ :
    - với 2 hàm này 2 cột là size và color sẽ được append vào sau cùng của posttype :

    thêm action trên vào construct của file {class-tên_plugin-admin.php} với action cho tên post type : 

   'manage_{tên post type}_posts_custom_column'

        add_action( 'manage_frame_posts_custom_column' , array($this,'custom_frame_column'), 10, 2 );

   -sau đó thêm các function sau vào cùng file đó : 


        public function custom_frame_column($column, $post_id ) {
            switch ( $column ) {

                case 'size' :
                    echo  get_post_meta(  $post_id , 'frame_size', true);               
                    break;

                case 'color' :
                    echo get_post_meta( $post_id , 'frame_color' , true ); 
                    break;
            }
        }


        public function custom_columns_frame($columns) {
            
            $columns['size'] = __( 'Size', 'frame' );
            $columns['color'] = __( 'Color', 'frame' );
            
            return $columns;
        }

   - muốn reorder lại các cột và tùy chọn hiển thị các côt như thế nào ta chỉ việc lặp lại như sau :


        public function custom_columns_frame($columns) {
    		// có thể unset các cột
            unset(
                $columns['ID']
            )
            $columns['size'] = __( 'Size', 'frame' );
    		$columns['color'] = __( 'Color', 'frame' );
            // đây là custom lại thứ tự hiển thị của các column trong post type
    		$customOrder = array( 'title',  'frame', 'color', 'date');
            /*
            * return a new column array to wordpress.
            * order is the exactly like you set in $customOrder.
            */
            foreach ($customOrder as $column_name)
                $rearranged[$column_name] = $columns[$column_name];
            return $rearranged;
    	}


*  function sortable cho column tạo mới trong posttype
    
    -đây là ví dụ cho việc set cho column mới thêm nào của chúng ta có thể sort được : 

    với action sau khi add vào function 'define_admin_hooks' của file {includes/class-tên_plugin.php} :
    cùng với filter mà chúng ta tạo ra với custom post type : 'manage_edit-{tên post type}_sortable_columns'


        $this->loader->add_filter( 'manage_edit-artist_sortable_columns',$plugin_admin, 'set_custom_artist_sortable_columns' );
        
   và hàm callback sau :
    
   


        public function set_custom_frame_sortable_columns($columns) {
            $columns['size'] = 'size';
            $columns['color'] = 'color';
            return $columns;
        }
        
   là ta đã chọn được ra column nào sẽ có thể sortable trong custom post type

   * sau đó chúng ta cần action pre get posts để custom sortable cho các column đã chọn

   - thêm action sau vào function 'define_admin_hooks' của file {includes/class-tên_plugin.php} : 
   

        $this->loader->add_action( 'pre_get_posts', $plugin_admin, 'custom_posts_orderby' );

   - cùng hàm call back trong file {class-tên_plugin-admin.php} : 


        public function custom_posts_orderby($query) {
            if ( ! is_admin() )
                return;

            $orderby = $query->get( 'orderby');

            // frame
            if ( 'size' == $orderby ) {
                $query->set( 'meta_key', 'frame_size' );
                $query->set( 'orderby', 'meta_value' );
            }
            if ( 'color' == $orderby ) {
                $query->set( 'meta_key', 'frame_color' );
                $query->set( 'orderby', 'meta_value' );
            }
        }
        
        
   - hàm callback trên là hành động custom lại query get posts của wordpress với các kiểu orderby khác nhau
    có thể order by meta_key , terms, meta_value , v.v
