# THÊM METABOX CHO POST / PAGE TRONG ADMIN SIDE #

1. add_meta_box( id, title, callback, screen, context, priority, callback_args )

   * id : id của metabox  - là 1 dãy string unique, hãy đặt tên duy nhất cho mebox khác nhau
   * title : Title của 1 cái metabox 
   * callback : hàm callback để hiển thị hoặc xử lý cho metabox đã tạo ra
   * screen : metabox này hiển thị cho loại posttype nào : post, page, custompostype , v.v
   * context : thường có 3 kiểu 'normal', 'side', and 'advanced' thử sẽ thấy sự khác biệt
   * priority : độ ưu tiên cho metabox hiển thị lên trên hay dưới
   * callback_args : data có thể truyền vào metabox hay nói các khác là các arguments custom có thể truyền vào để thay đổi trong metabox tạo ra v.v

    - ví dụ : thêm nhiều metabox trong 1 action hook

 - THÊM 2 ACTION sau vào file {includes/class-tên_plugin.php} trong phần "define_admin_hooks" :

   * thêm action với calback là hàm add metabox
   

        $this->loader->add_action( 'admin_init', $plugin_admin, 'rerender_meta_options' );
        
   * thêm action  khi xảy ra sự kiện save post để xử lý data khi đã tạo với metabox

   
	    $this->loader->add_action( 'save_post', $plugin_admin, 'save_meta_options' );

   * Đây chính là hàm callback để tạo các metabox cho các posttype 
   

    	public function rerender_meta_options() {
    		
    		add_meta_box("frame-meta", "Frame Details", array($this, "display_frame_meta_options"), "frame", "side","high");
    
    		add_meta_box("frame-meta-region", "Display Region In Frame", array($this, "display_frame_inside_meta_options"), "frame", "side","low");
    
    		add_meta_box("artist-meta", "Artist Details", array($this, "display_artist_meta_options"), "artist", "side", "high");
    
    		add_meta_box("artist-artwork-meta", "Artwork Details", array($this, "display_artist_artwork_meta_options"), "artist", "normal", "low");
    
    		add_meta_box("img-control-product-meta", "Artist Owned", array($this, "display_product_meta_options"), "product", "normal", "high");
    
            action này có thể hook vào trong phần hiển thị của woocommerce
            
    		add_action( 'woocommerce_product_options_general_product_data', array($this,'woo_add_custom_general_fields_artist' ));
    	}



   -  Đây là callback hiển thị cho  1 metabox
   -  Display meta box and custom fields
 

    	public function display_frame_meta_options() {
    
    		global $post;
    		$custom = get_post_custom($post->ID);
    		$frame_size="";
    		$frame_color="";
    		$frame_price = "";
    		if (isset($custom['frame_size']))	$frame_size = $custom['frame_size'][0];
    		if (isset($custom['frame_color']))	$frame_color = $custom['frame_color'][0];
    		if (isset($custom['frame_price']))	$frame_price = $custom['frame_price'][0];
    	
    		
    		<div class="input-group" style="padding:3px;margin:3px;">
    			<span>Price... : </span>
    			<input type="text" class="form-control" name="frame_price" id="price_frame" placeholder="Price" value="<?=$frame_price?>">
    			<b><?=get_option('woocommerce_currency');?></b>
    		</div>
    		<div class="input-group" style="padding:3px;margin:3px;">
    			<span>Size : </span>
    			<input type="text" class="form-control" name="frame_size" id="size_frame" placeholder="Size" value="<?=$frame_size?>">
    			
    		</div>
    		<div class="input-group" style="padding:3px;margin:3px;">
    			<span>Color : </span>
    			<input type="text" class="form-control" name="frame_color" id="color_frame" placeholder="Color" value="<?=$frame_color?>">
    		</div> 
	
    
    	} 
    	
    	
   *  Xử lý cho việc save post , add data từ metabox
   ví dụ  update cho post này 1 cái meta mà ta đã tạo ở trên

        public function save_meta_options() {
            	global $post;
            	if (isset($post)) $post_id = $post->ID;
            	if (isset($_POST["frame_size"])) {
            			update_post_meta($post_id, "frame_size", sanitize_text_field($_POST["frame_size"]));
            	}
        }
    

