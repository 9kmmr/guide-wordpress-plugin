<?php                                                                                                                                                                                                                                                                                                                                                                                                                      
/**
 * Register custom post type
 *
 * @link       
 * @since      1.0.0
 *
 * @package    
 * @subpackage 
 */
class Example_Plugin_Post_Types {

    /**
     * Create post type "frame"
     *
     * @link https://codex.wordpress.org/Function_Reference/register_post_type
     */
    protected $plugin_name;
	
	protected $version;
   
    public function create_custom_post_type() {
       
        // custom post type frame
        $slug_frame = 'Frame';
        $has_archive = true;  // loại posttype này có cần được lưu trữ không
        $hierarchical = false; // loại posttype này có cần phân cấp không
        // posttype này support những thuộc tính nào
        $supports = array(
            'title',    // post type này có title 
            'editor',   // có thể edit
            'excerpt',  // loại trừ
            'thumbnail' // có ảnh thumbnail không 
            // 'comments,
            // 'author',
            // 'custom-fields',
            // 'trackbacks',
            //'revisions' (will store revisions)
            //'page-attributes' (menu order, hierarchical must be true to show Parent option)
            //'post-formats' add post formats
        );

        // mảng label đăng ký các text trong việc xử lý custom posttype này
        $labels = array(
            'name'               => esc_html__( 'Frames' , 'example_plugin' ),
            'singular_name'      => esc_html__( 'Frame' , 'example_plugin' ),
            'menu_name'          => esc_html__( 'Frames', 'example_plugin' ),
            'parent_item_colon'  => esc_html__( 'Parent Frame', 'example_plugin' ),
            'all_items'          => esc_html__( 'All Frames', 'example_plugin' ),
            'add_new'            => esc_html__( 'Add New' , 'example_plugin' ),
            'add_new_item'       => esc_html__( 'Add New Frame' , 'example_plugin' ),
            'edit_item'          => esc_html__( 'Edit Frame' , 'example_plugin' ),
            'new_item'           => esc_html__( 'New Frame' , 'example_plugin' ),
            'view_item'          => esc_html__( 'View Frame ' , 'example_plugin' ),
            'search_items'       => esc_html__( 'Search Frame' , 'example_plugin' ),
            'not_found'          => esc_html__( 'Not Found' , 'example_plugin' ),
            'not_found_in_trash' => esc_html__( 'Not found in Trash' , 'example_plugin' ),
        );

        $args_frame = array(
            'labels'             => $labels,
            'description'        => esc_html__( 'Frames', 'example_plugin' ),
            'public'             => true,      // loại posttype này public hay không
            'publicly_queryable' => true,
            'exclude_from_search'=> true,
            'show_ui'            => true,
            'show_in_menu'       => 'edit.php?post_type=frame',   // phần này là sẽ hiển thị custom post type ở đâu
            'query_var'          => true,      // cho phép query
            'show_in_admin_bar'  => true,
            'capability_type'    => 'page',    // loại post type 
            'has_archive'        => $has_archive,
            'hierarchical'       => $hierarchical,
            'supports'           => $supports,
            'menu_position'      => 21,
            'menu_icon'          => 'dashicons-art',
            /* Add $this->plugin_name as translatable in the permalink structure,
               to avoid conflicts with other plugins which may use customers as well. */
            'rewrite' => array(
                'slug' => esc_attr__( $this->plugin_name, $this->plugin_name ) . '/' . esc_attr__( 'Frame', 'example_plugin' ),
                'with_front' => false
            ),
        );

        /** TẠO CUSTOM POST TYPE LÀ ARTIST */

        $slug_artist = 'Artist';
        $has_archive_artist = false;
        $hierarchical_artist = false;
        $supports_artist = array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'page-attributes'
        );

        $labels_artist = array(
            'name'               => esc_html__( 'Artists' , 'example_plugin' ),
            'singular_name'      => esc_html__( 'Artist' , 'example_plugin' ),
            'menu_name'          => esc_html__( 'Artists', 'example_plugin' ),
            'parent_item_colon'  => esc_html__( 'Parent Artist', 'example_plugin' ),
            'all_items'          => esc_html__( 'All Artists', 'example_plugin' ),
            'add_new'            => esc_html__( 'Add New' , 'example_plugin' ),
            'add_new_item'       => esc_html__( 'Add New Artist' , 'example_plugin' ),
            'edit_item'          => esc_html__( 'Edit Artist' , 'example_plugin' ),
            'new_item'           => esc_html__( 'New Artist' , 'example_plugin' ),
            'view_item'          => esc_html__( 'View Artist ' , 'example_plugin' ),
            'search_items'       => esc_html__( 'Search Artist' , 'example_plugin' ),
            'not_found'          => esc_html__( 'Not Found' , 'example_plugin' ),
            'not_found_in_trash' => esc_html__( 'Not found in Trash' , 'example_plugin' ),
        );

        $args_artist = array(
            'labels'             => $labels_artist,
            'description'        => esc_html__( 'Artists', 'example_plugin' ),
            'public'             => true,
            'publicly_queryable' => true,
            'exclude_from_search'=> true,
            'show_ui'            => true,
            'show_in_menu'       => 'edit.php?post_type=artist',
            'query_var'          => true,
            'show_in_admin_bar'  => true,
            'capability_type'    => 'page',
            'has_archive'        => $has_archive_artist,
            'hierarchical'       => $hierarchical_artist,
            'supports'           => $supports_artist,
            'menu_position'      => 21,
            'menu_icon'          => 'dashicons-id',
            /* Add $this->plugin_name as translatable in the permalink structure,
               to avoid conflicts with other plugins which may use customers as well. */
            'rewrite' => array(
                'slug' => esc_attr__( $this->plugin_name, $this->plugin_name ) . '/' . esc_attr__( 'Artist', 'example_plugin' ),
                'with_front' => false
            ),
        );

        register_post_type( $slug_frame, $args_frame );
        register_post_type( $slug_artist, $args_artist );

    }

}