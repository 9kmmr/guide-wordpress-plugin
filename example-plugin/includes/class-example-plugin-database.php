<?php


class Example_Plugin_Database {
    protected $plugin_name;	
    protected $version;
    private $example_plugin_version = 1.01;


    /**
     * install db
     */
    public function canabis_md_db_install() {
        global $wpdb;       

        $table_name = $wpdb->prefix . 'example_plugin';

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id  INT NOT NULL AUTO_INCREMENT,
            fullname  TEXT NOT NULL ,
            domain TEXT  NOT NULL,
            email TEXT NOT NULL,
            phonenumber TEXT  NOT NULL,
            datecreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            approved INT DEFAULT '0' NOT NULL,
        
            PRIMARY KEY  (id)
            ) $charset_collate;       

        ";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );    
        add_option( 'example_plugin_db_version', $this->example_plugin_version );
    }

    /**
     * uninstall
     */

    public function canabis_md_db_uninstall() {
        global $wpdb;      

        $table_name = $wpdb->prefix . 'example_plugin';

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "DROP TABLE IF EXISTS ".$table_name ." ; " ;     

        $wpdb->query( $sql );     

        delete_option('example_plugin_db_version');
    }
}