# TẠO XÓA CUSTOM DATABASE TRONG WORDPRESS #

### TẠO MỚI 1 DATABASE ###

1. Yêu cầu ở đây là tạo 1 table riêng trong database  mà ta có thể thực hiện trong plugin

    * hướng làm ở đây là ta sẽ tạo 1 table ngay khi lúc active plugin và sẽ xóa table sau khi deactive hoặc xóa plugin
    * ở đây ta có 2 cách làm : 
    1 là viết hàm tạo database ngay trong activator và xóa db trong deactivator
    2 là tạo ra 1 class riêng chuyên biệt để tạo db và xóa db cho dễ maintain

    Hướng dấn sẽ theo cách thứ 2.

2. Các bước thực hiện : 

    * tạo 1 file trong thư mục includes tên {class-tên_plugin-database.php}
      tạo 1 classname tùy ý và các property tùy ý và 2 hàm sau : 

    ví dụ hàm tạo database:


        public function canabis_md_db_install() {
            global $wpdb;   // biến global của wordpress thao tác với db    

            $table_name = $wpdb->prefix . 'example_plugin';  // tạo tên table

            $charset_collate = $wpdb->get_charset_collate();  // lấy charset của database

            // đoạn này là query tạo table
            $sql = "CREATE TABLE $table_name (
                id  INT NOT NULL AUTO_INCREMENT,
                fullname  TEXT NOT NULL ,
                domain TEXT  NOT NULL,
                email TEXT NOT NULL,
                phonenumber TEXT  NOT NULL,
                datecreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                approved INT DEFAULT '0' NOT NULL,
            
                PRIMARY KEY  (id)
                ) $charset_collate;       

            ";
            // chúng ta sử dụng dbDelta để update lại database
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );    
            // tạo option cho database version để sau có thể update database
            add_option( 'example_plugin_db_version', $this->example_plugin_version );
        }  

    * ví dụ hàm xóa database  

        public function canabis_md_db_uninstall() {
            global $wpdb;      

            $table_name = $wpdb->prefix . 'example_plugin';

            $charset_collate = $wpdb->get_charset_collate();

            $sql = "DROP TABLE IF EXISTS ".$table_name ." ; " ;     

            $wpdb->query( $sql );     

            delete_option('example_plugin_db_version');
        }

3. sau khi đã có class trên và các hàm trong đó tiếp theo là chúng ta sử dụng các hàm và class này ở đâu như thế nào

    * đầu tiên ta vào file {tên_plugin.php}  ở thư mục plugin ngoài cùng
    * ta có thể thấy được 2 hàm deactive và active plugin và 2 action hook
        register_activation_hook( __FILE__, 'activate_example_plugin' );
        register_deactivation_hook( __FILE__, 'deactivate_example_plugin' );
    đây chính là 2 action thực hiện đầu tiên ngay khi active hay deactive plugin

    * tiếp theo khi ta nhìn vào 2 hàm active hay deactive ta thấy require 2 file class-xx-activator và class-xx-deactivator và chạy hàm activate() và deactivate() tức nghĩa là ta phải chạy tạo db và xóa db ở trong 2 hàm active() và deactivate() trong class-example-plugin-activator.php và class-example-plugin-deactivator.php

        require_once plugin_dir_path( __FILE__ ) . 'includes/class-example-plugin-activator.php';
        Example_Plugin_Activator::activate();
        require_once plugin_dir_path( __FILE__ ) . 'includes/class-example-plugin-deactivator.php';
        Example_Plugin_Deactivator::deactivate();
    
    



