<?php

/**
 * Fired during plugin deactivation
 *
 * @link       yourmindhasgone.co
 * @since      1.0.0
 *
 * @package    Example_Plugin
 * @subpackage Example_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Example_Plugin
 * @subpackage Example_Plugin/includes
 * @author     lamvu <yourmindhasgone@gmail.com>
 */
class Example_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
