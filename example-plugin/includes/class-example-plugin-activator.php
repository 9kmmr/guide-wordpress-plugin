<?php

/**
 * Fired during plugin activation
 *
 * @link       yourmindhasgone.co
 * @since      1.0.0
 *
 * @package    Example_Plugin
 * @subpackage Example_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Example_Plugin
 * @subpackage Example_Plugin/includes
 * @author     lamvu <yourmindhasgone@gmail.com>
 */
class Example_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
