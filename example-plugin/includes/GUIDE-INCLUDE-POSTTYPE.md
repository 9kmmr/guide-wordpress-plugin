# REGISTER CUSTOM POST TYPE #

1. Yêu cầu ở đây là chúng ta muốn tạo ra vài cái custom post type khi active plugin tức nghĩa là ta phải hook vào action active plugin, như vậy như phần thêm database ta xử lý trên file class-{tên_plugin}-activator.php

    * ta tạo 1 file trong thư mục include tên là class-{tên_plugin}-custom_post_type.php
    hướng dẫn xử lý trong file
    tiếp theo ta phải require file đó vào phần active của plugin: thêm vào hàm active()


        /**
		 * Custom Post Types
		 */
         // require file chứa phần đăng ký custom posttype
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-example-plugin-custom_post_types.php';
        // khởi tạo class
		$plugin_post_types = new Example_Plugin_Post_Types();

		/**
		 * The problem with the initial activation code is that when the activation hook runs, it's after the init hook has run,
		 * so hooking into init from the activation hook won't do anything.
		 * You don't need to register the CPT within the activation function unless you need rewrite rules to be added
		 * via flush_rewrite_rules() on activation. In that case, you'll want to register the CPT normally, via the
		 * loader on the init hook, and also re-register it within the activation function and
		 * call flush_rewrite_rules() to add the CPT rewrite rules.
		 *
		 * @link https://github.com/DevinVinson/WordPress-Plugin-Boilerplate/issues/261
		 */
         // chạy hàm tạo post type trong class
		$plugin_post_types->create_custom_post_type();

		flush_rewrite_rules();
	

