# ĐĂNG KÝ SHORT CODE ĐỂ SỬ DỤNG #

# CÓ THỂ HIỂU SHORT CODE LÀ 1 TRANG RIÊNG CÓ THỂ NHÚNG VÀO BẤT CỨ ĐÂU CHỈ VIỆC GỌI ĐẾN TÊN CỦA NÓ #
1. Đăng ký shortcode tĩnh 

    * Thêm action add_shortcode cho mỗi shortcode cần thêm
    * Ta có thể thêm action add_shortcode vào public hoặc admin

        // SHORT CODE
		$this->loader->add_shortcode( "tenshortcode", $plugin_public, "hamxuly" );


    * trong đó tenshortcode chính là tên shortcode mà chúng ta khai báo, lưu ý không có dấu cách
    * hamxuly chính là hàm callback trong đó xử lý các parametter chúng ta truyền vào và hiển thị khi ta nhúng vào post, page v.v 
    - ví dụ

            function shortcode_function( $atts ) {
                // attribute hay còn gọi là các parametter của shortcode , có thể có hoặc không tùy chúng ta xử lý 
                // tài liệu : https://codex.wordpress.org/Function_Reference/shortcode_atts
                $args = shortcode_atts(
                    array(
                        'arg1'   => 'arg1',
                        'arg2'   => 'arg2',
                    ),
                    $atts
                );
                // code...
                // xử lý post, get data, blable v.v
                // xử lý với các parametter bên trên
                // code...
                // in ra html v.v
               
            }

    * cách xử dụng khi nhúng vào post hay page : có 2 dấu ngoặc vuông bao quanh shortcode :  [tenshortcode arg1="" arg2= ""] 
    * trong đó arg1/ arg2 là 2 parametter truyền vào, có thể có 0,1,2,3 v.v parametter ta có thể truyền vào


2. Đăng ký shortcode động 

