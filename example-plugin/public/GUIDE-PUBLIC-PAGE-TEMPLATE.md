# ĐĂNG KÝ PAGE TEMPLATE #

### Yêu cầu : Ta cần 1 page riêng theo ta design, không phụ thuộc vào theme   ###

1. Tạo page template 

    *thêm 2 filter vào includes/class-{teenplugin}.php trong phần public
            // add template test speed page	
		
            $this->loader->add_filter('theme_page_templates', $plugin_public, 'add_page_template', 10,4);	
                
            $this->loader->add_filter('template_include', $plugin_public, 'load_page_template');

            // tạo thư mục template và file template đặt ở thư mục đó, mỗi template 1 file
            /**
            * register page template test_speed_page
            */
            public function add_page_template( $templates ) {
                // Add custom template named template-custom.php to select dropdown 
                $post_templates['page_template.php'] = __('Example Page Template');

                return $post_templates;
            }
            public function load_page_template( $template ) {
                $templates_dir = 'templates';
                if(  get_page_template_slug() === 'page_template.php' ) {

                    if ( $theme_file = locate_template( array( 'page_template.php' ) ) ) {
                        $template = $theme_file;
                    } else {
                        $template = WP_PLUGIN_DIR . '/' . $this->plugin_name . '/' . $templates_dir . '/page_template.php';
                    }
                }

                if($template == '') {
                    throw new \Exception('No template found');
                }

                return $template;
            }