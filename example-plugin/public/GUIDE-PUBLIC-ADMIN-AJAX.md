# ĐĂNG KÝ VÀ CHẠY AJAX #

1. Yêu cầu : xử lý hành động với ajax

    * Đăng ký action để thực hiện trong admin-ajax.php
    * giải thích : tất cả các hành động chạy ajax trong wordpress đều cần xử lý qua file admin-ajax.php , nên chúng ta cần đăng ký hành động với file này để có thể thực hiện ajax trong 1 site wordpress

    * Chúng ta có thể register ajax ở bất cứ đâu : cả trong dashboard lẫn ngoài front page

    * tùy vào mục đích sử dụng ở đâu thì ta nên thêm action vào define_public_hooks() hay define_admin_hooks():
    * ví dụ sau muốn hiển thị ở ngoài front page nên đặt ở trong public_hooks, mục đích là muốn hiển thị cái custom post type là frame lên : 

        $this->loader->add_action('wp_ajax_get_frame_example_plugin_ajax', $plugin_public, 'get_frame_example_plugin_ajax');
		$this->loader->add_action('wp_ajax_nopriv_get_frame_example_plugin_ajax', $plugin_public, 'get_frame_example_plugin_ajax'); // Not logged in user
    
    * chú ý 
    * phần đăng ký tên action gồm 2 phần : 
        - 1 là "wp_ajax_"  |  nếu muốn xử dụng cho user chưa login thì phân 1 của action sẽ là  "wp_ajax_nopriv_" 
        - 2 là tên action mà chúng ta sẽ gọi đến ở đây tên là "get_frame_example_plugin_ajax"
    *  vậy trong class-{tên_plugin}-public.php cần phải thêm 1 function get_frame_example_plugin_ajax và trả về giá trị mà ta cần xử lý


    * Trong khi đó: muốn gọi đến để lấy data ra thì ta cần phải xử dụng 1 ajax request trong đó data truyền vào có 1 trường là action với tên chính là cái tên action chúng ta đặt. Ví dụ : 

        var dataJSON = {
			'action': 'get_frame_example_plugin_ajax',    // đây chính là tên action ta đặt
            // 2 data dưới là các thông tin ta post vào hàm để xử lý
			'frame_size': 	$('#select_frame_size_product').val(),
			'frame_color': ''
		};
		
		$.ajax({
				cache: false,
				type: "POST",
				url: wp_ajax.ajax_url, MUỐN SỬ DỤNG LINK NHƯ THẾ NÀY TA CẦN THEO DÕI PHẦN  (2) 
				data: dataJSON, // DATA TRUYỀN VÀO 
				success: function( response ){
                }
        })
         
    (2) BÌNH THƯỜNG TA PHẢI REQUEST TỚI file admin-ajax.php và không biết rõ được là họ đặt thư mục của website như thế nào nên rất khó lấy được url path của file admin-ajax.php, trong backend biến ajaxurl đã được tự động defined nhưng ở front end thì ta phải tự define

    *  ta có cách để lấy dynamic link tới admin-ajax.php ở front end
    - thêm những đoạn code sau vào hàm enqueue_scripts() trong file /public/class-{tên_plugin}-public.php


            /**
            *  In backend there is global ajaxurl variable defined by WordPress itself.
            *
            * This variable is not created by WP in frontend. It means that if you want to use AJAX calls in frontend, then you have to define such variable by yourself.
            * Good way to do this is to use wp_localize_script.
            *
            * @link http://wordpress.stackexchange.com/a/190299/90212
            */
            wp_localize_script( $this->plugin_name, 'wp_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    - sau đó ta có thể dùng url như trên ví dụ trên
    * vậy ta đã xong các bước đăng ký action để chạy ajax với admnin-ajax.php


        
		